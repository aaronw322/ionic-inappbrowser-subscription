import {Component} from '@angular/core';
import {InAppBrowser, InAppBrowserEventType, InAppBrowserOptions} from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private iab: InAppBrowser) {
  }

  openInAppBrowser() {
    const exampleUrl = 'https://ionicframework.com/docs/components';
    const options: InAppBrowserOptions = {
      usewkwebview: 'yes'
    };
    const browser = this.iab.create(exampleUrl, '_blank', options);
    const eventTypes: InAppBrowserEventType[] = ['loadstart', 'loadstop', 'beforeload', 'exit', 'loaderror'];
    eventTypes.forEach(eventType => {
      browser.on(eventType).subscribe(event => {
        console.log(`Event ${eventType} fired!`); // Doesn't get here
        console.log(`URL: [${event.url}]`);       // or here (obviously).
        if (event.url && event.url.includes('path-i-am-interested-in')) {
          // Do useful stuff...
        }
      });
    });
    console.log(`Browser object after subscription:`);
    console.log(browser);
  }
}
