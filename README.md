# Setup
This just demonstrates how the project was created - this script doesn't need to run again.
```
nvm list
->     v10.16.0

ionic --version
6.12.1

ionic start iabApp --type=angular blank
? Integrate your new app with Capacitor to target native iOS and Android? (y/N) y

cd iabApp

# InAppBrowser with Capacitor
npm install cordova-plugin-inappbrowser
+ cordova-plugin-inappbrowser@4.0.0

npm install @ionic-native/in-app-browser
+ @ionic-native/in-app-browser@5.29.0
```

# Build
```
cd iabApp

ionic build

ionic cap sync

npx cap add ios

npx cap open ios
```
